var base_url=$("#base_url").val();
$(document).ready(function () {   
    loadTable();

    $("#fechai,#fechaf").on("change",function(){
        if($("#fechai").val()!="" && $("#fechaf").val()!="")
            loadTable();
    });
    /*$("#categoria").on("change",function(){
        if(this.value!= undefined)
            loadTable();
    });
    $("#servicio").on("change",function(){
        if(this.value!= undefined)
            loadTable();
    });
    search_familia();
    get_servicios();*/
});

/*function search_familia(){
    $('#categoria').select2({
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ordenes/searchFamilias',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre,
                        id_emp: element.id_emp
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        var ide = $("·ide").val();
        $("#categoria option:selected").attr('data-emp',ide);
        get_servicios();
    });
}

function get_servicios(){
    $('#servicio').select2({
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ordenes/searchServicios',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                    fam: $("#categoria option:selected").val()
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    rec="";
                    switch (element.recipientes){
                        case 1: rec="CAT1"; break;
                        case 2: rec="CAT2"; break;
                        case 3: rec="CAT3"; break;
                    }
                    itemscli.push({
                        id: element.id,
                        text: element.nombre+" "+rec,
                        id_emp: element.id_emp
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;

    });
}*/

function loadTable(){
    url = "https://ecose.mangoo.systems/reportes/cliente/";
    //url = "../../ecose/reportes/cliente/";
    var categoria=$("#categoria option:selected").val();
    if(categoria==undefined){ categoria=0; }
    var servicio=$("#servicio option:selected").val();
    if(servicio==undefined){ servicio=0; }

    //console.log("categoria: "+categoria);
    //console.log("servicio: "+servicio);
    var table = $('#tabla').DataTable({
        responsive: true,
        "bProcessing": true,
        "serverSide": true,
        searching:true,
        destroy:true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            url: base_url+"Ordenes/getData_ordenes",
            data: { fechai:$("#fechai").val(), fechaf:$("#fechaf").val()/*, categoria:categoria,servicio:servicio*/ },
            type: "post",
        },

        "columns": [
            {"data": "cotizacion_id"},
            {"data": "alias"},
            {"data": "fecha_creacion"},
            //{"data": "servicio"},
            {"data": "servicio",
                "render": function (data, type, row, meta) {
                    var servname=""; var serv_ant="";
                    servicios_consul=row.servicios;
                    idserviciosconsul=row.idservicios;
                    var idserviciobr_ant=0;
                    //console.log("--- servicios_consul: "+servicios_consul);
                    //console.log("--- idserviciosconsul: "+idserviciosconsul);
                    if(servicios_consul!=null){
                        var word = $('input[type=search]').val();                       
                        idserviciobr=idserviciosconsul.split('<br>');
                        idserviciobr = (idserviciobr.filter( onlyUnique ));
                        serv_br=servicios_consul.split('<br>');
                        serv_br = serv_br.filter( onlyUnique );

                        //idserviciobr.sort();
                        //serv_br.sort();

                        /*idserviciobr.sort(function(a, b) {
                            return a - b;
                        });
                        serv_br.sort(function(a, b) {
                            return a - b;
                        });*/

                        //console.log("--- idserviciobr: "+idserviciobr);
                        //console.log("--- serv_br: "+serv_br);
                        for (var i=0; i<serv_br.length; i++) {
                            /*console.log("--- id cotiza: "+row.cotizacion_id);
                            console.log("servicio: "+serv_br[i]);
                            console.log("serv_ant: "+serv_ant);*/
                            if(serv_br[i]!=serv_ant){
                                name_class=serv_br[i].replace(/ /g, "_");
                                name_class2=name_class.replaceAll(",", "");
                                name_class3=name_class2.replaceAll("-", "_");
                                //name_class3=name_class3.replace(/\d+/g, '');
                                name_class3=name_class3.replaceAll("(", "");
                                name_class3=name_class3.replaceAll(")", "");
                                name_class3=name_class3.replaceAll("/", "");
                                //name_class3=name_class3.replaceAll("_", " ");
                                name_class4=eliminarDiacriticos(name_class3);
                                //console.log("name_class4: "+name_class4);
                                if(serv_br[i].includes(word) && serv_br[i]!=""){
                                    $('.'+name_class4).show();
                                    var class_servicio='';
                                }else if(serv_br[i].includes(word)==false && serv_br[i]!=""){
                                    //console.log("name_class4 a ocultar: "+name_class4);
                                    var class_servicio='ocultar';
                                    $('.'+name_class4).remove();
                                    $('.'+idserviciobr[i]).remove();
                                }
                                //console.log("idserviciobr: "+idserviciobr[i]);
                                //console.log("serv_br: "+serv_br[i]);
                                if(name_class4!=""){
                                    servname += "<p class='"+idserviciobr[i]+"'></p><span class='"+name_class4+" "+class_servicio+"' id='"+idserviciobr[i]+"_"+row.cotizacion_id+"'>"+serv_br[i]+"<br></span>";
                                }
                            }
                            //console.log("idserviciobr_ant: "+idserviciobr_ant);
                            //console.log("idserviciobr [i]: "+idserviciobr[i]);
                            if(idserviciobr_ant==idserviciobr[i]){
                                //$("#"+idserviciobr[i]+"_"+row.cotizacion_id).hide();
                            }
                            serv_ant = serv_br[i];
                            idserviciobr_ant = idserviciobr[i];
                        }
                    }

                    /*id_serv_reporbr=row.id_serv_repor.split('<br>');
                    servicio_idchsbr=row.servicio_idchs.split('<br>');
                    id_serv_reporbr.sort();
                    servicio_idchsbr.sort();                    
                    var aux1 = id_serv_reporbr.map(function(el) { return JSON.stringify(el); });
                    var aux2 = servicio_idchsbr.map(function(el) { return JSON.stringify(el); });*/

                    id_serv_cargabr=row.id_serv_carga.split('<br>');
                    servicio_idchs2br=row.servicio_idchs2.split('<br>');
                    //console.log("id_serv_cargabr "+id_serv_cargabr);
                    //console.log("servicio_idchs2br: "+servicio_idchs2br);
                    //id_serv_cargabr.sort();
                    //servicio_idchs2br.sort();                    
                    var aux1 = id_serv_cargabr.map(function(el) { return JSON.stringify(el); });
                    var aux2 = servicio_idchs2br.map(function(el) { return JSON.stringify(el); });

                    // añadimos todas las cadenas del primer array que no estén en el segundo
                    var data3 = aux1.filter(function(el) {
                        if (aux2.indexOf(el) < 0) return el;
                    });
                    // añadimos todas las cadenas del segundo array que no estén en el primero
                    data3 = data3.concat( aux2.filter(function(el) {
                        if (aux1.indexOf(el) < 0) return el;
                    }));
                    // convertimos las cadenas de nuevo a objetos
                    data3 = data3.map(function (el) { return JSON.parse(el); });
                    console.log(data3);
                    //$("#"+data3+"_"+row.cotizacion_id).css("display","none");
                    $("#"+data3+"_"+row.cotizacion_id).hide();
                    return servname;
                }
            },
            {"data": null,
                "render": function (data, type, row, meta) {            
                    /*var html=""; var servname=""; var serv_ant=""; var file_ant="";
                    file_repor2=row.file_repor2;
                    id_repor=row.id_repor;
                    servicio_idchs=row.servicio_idchs;
                    id_sr=row.id_serv_repor;
                    var serv_rep_ant=0;
                    if(file_repor2!=null){
                        file_br=file_repor2.split('<br>');
                        id_br=id_repor.split('<br>');
                        servicio_idchs=servicio_idchs.split('<br>'); //servicios de la cotizacion
                        id_sr=id_sr.split('<br>'); //servicios que se asignaron un reporte
                        for (var i=0; i<file_br.length; i++) {
                            //validar que exista documento asignado al servicio, que sean iguales para ponerlo a un lado, si no hacer saltos de linea
                            //comparar servicio_idchs con id_sr
                            //console.log("file_br: "+file_br[i]);
                            //console.log("serv_ant: "+file_ant);
                            console.log("id_sr length: "+id_sr.length);
                            if(id_sr.length>=1){
                                if(getFileExtension3(file_br[i])=="pdf" && id_sr[i]==servicio_idchs[i] || getFileExtension3(file_br[i])=="pdf" && id_sr[i]==serv_rep_ant){
                                    html+='<a href="'+url+row.carpeta_prin+'/'+row.carpeta+'/'+file_br[i]+'" target="_blank" download title="'+file_br[i]+'"><img width="35px" src="'+base_url+'public/img/pdf.png"> </a>';
                                    //html+='<a href="javascript:void(0)" title="PDF" onclick="descargar('+row.id+','+id_br[i]+')"><img src="'+base_url+'public/img/pdf.png"> </a>';
                                }else if(getFileExtension3(file_br[i])!="pdf" && id_sr[i]==servicio_idchs[i] || getFileExtension3(file_br[i])!="pdf" && id_sr[i]==serv_rep_ant){
                                    html+='<a href="'+url+row.carpeta_prin+'/'+row.carpeta+'/'+file_br[i]+'" target="_blank" download title="'+file_br[i]+'"><img width="35px" src="'+base_url+'public/img/zip.png"> </a>';
                                    //html+='<a href="javascript:void(0)" title="ZIP" onclick="descargar('+row.id+','+id_br[i]+')"><img width="45px" src="'+base_url+'public/img/zip.png"> </a>';
                                }
                            }
                            if(getFileExtension3(file_br[i])=="pdf" && id_sr[i]!=servicio_idchs[i]){
                                html+='<br><a href="'+url+row.carpeta_prin+'/'+row.carpeta+'/'+file_br[i]+'" target="_blank" download title="'+file_br[i]+'"><img width="35px" src="'+base_url+'public/img/pdf.png"> </a>';
                                //html+='<a href="javascript:void(0)" title="PDF" onclick="descargar('+row.id+','+id_br[i]+')"><img src="'+base_url+'public/img/pdf.png"> </a>';
                            }else if(getFileExtension3(file_br[i])!="pdf" && id_sr[i]!=servicio_idchs[i]){
                                html+='<br><a href="'+url+row.carpeta_prin+'/'+row.carpeta+'/'+file_br[i]+'" target="_blank" download title="'+file_br[i]+'"><img width="35px" src="'+base_url+'public/img/zip.png"> </a>';
                                //html+='<a href="javascript:void(0)" title="ZIP" onclick="descargar('+row.id+','+id_br[i]+')"><img width="45px" src="'+base_url+'public/img/zip.png"> </a>';
                            }
                            serv_rep_ant = id_sr[i];
                            file_ant = file_br[i];
                            serv_ant = serv_br[i];
                        }
                    }
                    */
                    //html=getDocs(row.id,row.carpeta_prin,row.carpeta);
                    return getDocs(row.id,row.carpeta_prin,row.carpeta,row.id_serv_repor);
                }
            },
        ],
        columnDefs: [ { orderable: false, targets: [3,4] }],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    table.on('search.dt', function() {
        var word = $('input[type=search]').val();

        //console.log("buscar: "+word);
        /*var TABLA   = $("#tabla tbody > tr");
        TABLA.each(function(){
            nom = $(this).find("a[class*='cont_doc']").data("nom");
            name_class=nom.replace(/ /g, "_");
            name_class2=name_class.replaceAll(",", "");
            name_class2=name_class2.replaceAll("-", "_");
            name_class2=name_class2.replaceAll("(", "");
            name_class2=name_class2.replaceAll(")", "");
            //name_class2=name_class2.replace(/\d+/g, '');
            name_class2=eliminarDiacriticos(name_class2);
            //console.log("nom: "+nom);
            //console.log("name_class2: "+name_class2);
            
            if(nom.includes(word) ){
                //console.log("incluye la palabra: "+nom);
                //$("."+name_class).css("display","show");
                //$("a[class*='"+name_class2+"']").css("display","none");
                $('.'+name_class2).show();
                var class_servicio='tiene_servicio';
            }else{
                //$("."+name_class).css("display","none");
                //console.log("name_class2 a ocultar: "+name_class2);
                //$("a[class*='"+name_class2+"']").css("display","none");
                //$("a[class*='"+name_class2+"']").addClass("ocultar");
                var class_servicio='sin_servicio';
                $("."+name_class2).addClass(class_servicio);
                $('.'+name_class2).hide();
            }
            //console.log(class_servicio);
        });  */
    });
}
function eliminarDiacriticos(texto) {
    return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function getDocs(id_ord,carpeta_prin,carpeta,id_serv_repor){
    //console.log("names: "+names);
    url = "https://ecose.mangoo.systems/reportes/cliente/";
    //url = "../../ecose/reportes/cliente/";
    var html="";
    $.ajax({
        type: "POST",
        async: false,
        url: base_url+"Ordenes/getReportesDocs",
        data: {id_ord:id_ord, id_serv_repor:id_serv_repor},
        success: function (response) {
            var res = $.parseJSON(response);
            var id_sr_ant=0; var servicio_id_ant=0; var cont=0; var br=""; //var names=""; 
            var cotizacion_id_ant=0;
            res.datos.forEach(function(i){
                //console.log("nombre3: "+i.nombre3);
                //console.log("id_empresa_serv: "+i.id_empresa_serv);
                if(i.id_empresa_serv!=4 && i.id_empresa_serv!=5){
                    names=i.nombre;
                }if(i.id_empresa_serv==4){
                    names=i.nombre2;
                }if(i.id_empresa_serv==5){
                    names=i.nombre3;
                }

                /*console.log("files: "+i.file_repor);
                console.log("id_sr_ant: "+id_sr_ant);
                console.log("id_serv_repor: "+i.id_serv_repor);*/
                /*if(getFileExtension3(i.file_repor)=="pdf" && id_sr_ant!=i.id_serv_repor){
                    html+='<a href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="35px" src="'+base_url+'public/img/pdf.png"> </a>'+br;
                    //html+='<a href="javascript:void(0)" title="PDF" onclick="descargar('+id+','+i.id+')"><img src="'+base_url+'public/img/pdf.png"> </a>';
                }if(getFileExtension3(i.file_repor)!="pdf" && id_sr_ant!=i.id_serv_repor){
                    html+='<a href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="35px" src="'+base_url+'public/img/zip.png"> </a>';
                    //html+='<a href="javascript:void(0)" title="PDF" onclick="descargar('+id+','+i.id+')"><img src="'+base_url+'public/img/pdf.png"> </a>';
                }if(getFileExtension3(i.file_repor)=="pdf" && id_sr_ant==i.id_serv_repor ){
                    html+='<a href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="35px" src="'+base_url+'public/img/pdf.png"> </a>';
                    //html+='<a href="javascript:void(0)" title="ZIP" onclick="descargar('+row.id+','+i.id+')"><img width="45px" src="'+base_url+'public/img/pdf.png"> </a>';
                }if(getFileExtension3(i.file_repor)!="pdf" && id_sr_ant==i.id_serv_repor){
                    html+='<a href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="35px" src="'+base_url+'public/img/zip.png"> </a>';
                    //html+='<a href="javascript:void(0)" title="ZIP" onclick="descargar('+row.id+','+i.id+')"><img width="45px" src="'+base_url+'public/img/zip.png"> </a>';
                }*/
                var br="";
                name_class=names.replace(/ /g, "_");
                name_class2=name_class.replaceAll(",", "");
                name_class3=name_class2.replaceAll("-", "_");
                //name_class3=name_class3.replace(/\d+/g, '');
                name_class3=name_class3.replaceAll("(", "");
                name_class3=name_class3.replaceAll(")", "");
                name_class3=name_class3.replaceAll("/", "");
                name_class4=eliminarDiacriticos(name_class3);

                var word = $('input[type=search]').val();
                if(names.includes(word) ){
                    $('.'+name_class4).show();
                    var class_servicio='';
                }else{
                    //console.log("name_class4 a ocultar: "+name_class4);
                    var class_servicio='ocultar';
                }
                //console.log("servicio_id: "+i.servicio_id);
                //console.log("names: "+names);
                //console.log("id_serv_repor docs: "+id_serv_repor);
                if(getFileExtension3(i.file_repor)=="pdf" && servicio_id_ant!=i.servicio_id){
                    if(servicio_id_ant>0){
                        br="<p></p>";
                    }
                    html+= br+'<a class="form-group cont_doc '+class_servicio+'" href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="27px" src="'+base_url+'public/img/pdf.png"> </a>'; 
                }if(getFileExtension3(i.file_repor)!="pdf" && servicio_id_ant!=i.servicio_id){
                    if(servicio_id_ant>0){
                        br="<p></p>";
                    }
                    html+= br+'<a class="form-group cont_doc '+class_servicio+'" href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="24px" src="'+base_url+'public/img/zip.png"> </a>';
                }if(getFileExtension3(i.file_repor)=="pdf" && servicio_id_ant==i.servicio_id ){
                    html+='<a class="form-group cont_doc '+class_servicio+'" href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="27px" src="'+base_url+'public/img/pdf.png"> </a>';
                }if(getFileExtension3(i.file_repor)!="pdf" && servicio_id_ant==i.servicio_id){
                    html+='<a class="form-group cont_doc '+class_servicio+'" href="'+url+carpeta_prin+'/'+carpeta+'/'+i.file_repor+'" target="_blank" download title="'+i.file_repor+'"><img width="24px" src="'+base_url+'public/img/zip.png"> </a>';
                }
                id_sr_ant=i.id_serv_repor;
                servicio_id_ant=i.servicio_id;
                cont++;
            });
        
        }
    });
    return html;
}

function descargar(id,id_repor){
    uri_pri = "https://ecose.mangoo.systems/reportes/cliente/";
    $.ajax({
        type: "POST",
        url: "https://ecose.mangoo.systems/Ordenes/accessReport",
        success: function (data) {
        }
    });

    const xhr = new XMLHttpRequest();
    const url = uri_pri;
    xhr.open("GET", "https://ecose.mangoo.systems/reportes/cliente/",true);
    if (xhr != "User-Agent") {
        xhr.setRequestHeader('X-Alt-Referer', 'https://ecose.mangoo.systems/');
        xhr.setRequestHeader('Host' , 'https://ecose.mangoo.systems/');
        xhr.setRequestHeader('origin', 'https://ecose.mangoo.systems/');
        xhr.setRequestHeader('Referer', 'https://ecose.mangoo.systems/');;
    }
    xhr.onload = function () {
    if (xhr.status == 200) {

    } else {
        console.log("Error: "+xhr.status);
    }
    }
    xhr.send();
    
    //uri_pri = "http://localhost/ecose/reportes/cliente/";
    $.ajax({
        type: "POST",
        url: base_url+"Ordenes/getReporte",
        data: {id_ord:id, id_repor:id_repor},
        success: function (data) {
            var array=$.parseJSON(data);
            var contenido = uri_pri+array.carpeta_prin+"/"+array.carpeta+"/"+array.file_repor;
            //var contenido = array.file_repor;
            //console.log("contenido: "+contenido);
            //download(array.file_repor, uri_pri);

            // Crear un objeto Blob con el contenido
            //var blob = new Blob([contenido], { type: 'application/octet-stream' });

            // Crear una URL para el objeto Blob
            var url = decodeURIComponent(contenido);
            //var url = contenido;
            //console.log("url: "+url);
            // Crear un enlace para descargar el archivo
            var a = document.createElement('a');
            a.href = url;
            a.download = array.file_repor; // Nombre del archivo a descargar
            document.body.appendChild(a);

            // Simular un clic en el enlace para iniciar la descarga
            a.click();
            // Liberar la URL del objeto Blob
            //URL.revokeObjectURL(url);
        }
    });
}

function download(filename, textInput) {
    var element = document.createElement('a');
    element.setAttribute('href','data:text/plain;charset=utf-8, ' + encodeURIComponent(textInput));
    element.setAttribute('download', filename);
    document.body.appendChild(element);
    element.click();
      //document.body.removeChild(element);
}

function getFileExtension3(filename) {
    return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
}