var base_url=$("#basuri").val();
$(document).ready(function(){
    $("#password").keypress(function(e) {
        if(e.which == 13) {
            login();
        }
    });
    $("#login_btn").on("click",function(){
        login();
    });
});
function login(){
    if($("#usuario").val()=="" || $("#password").val()==""){
        $('#error').show();
    }
    else{
        $.ajax({
            type: "POST",
            traditional: true,
            url: base_url+"Main/iniciar_sesion",
            cache: false,
            data: {usuario: $("#usuario").val(), pass: $("#password").val(), idemp:$("#idemp").val()},
            success: function (data) {
                if (data==="ok"){   
                    $('#error').hide();
                    $('#correct').show();
                    setTimeout(function() { 
                        window.location = base_url+"Ordenes/inicio";
                    }, 1000);
                }
                else{
                    $('#error').show();
                }
            }
        }); // fin ajax
    }
}