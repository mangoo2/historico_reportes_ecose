<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Ordenes_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function searchFamilias($id_emp){
        $where="";
        $sel= '$id_emp'. "as id_emp";
        if($id_emp!=4 && $id_emp!=5){
            $tabla="servicios";
        }
        if($id_emp==4){
            $tabla="servicios_ahisa";
            $where="where f.id_empresa=$id_emp";
        }
        if($id_emp==5){
            $tabla="servicios_auven";
            $where="where f.id_empresa=$id_emp";
        }
        $sql = "SELECT f.nombre, f.id 
        FROM familias f INNER JOIN $tabla s ON f.id=s.familia "
        ."$where GROUP BY s.familia order by nombre asc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function searchServicios($familia,$empresa){
        if($empresa==1 || $empresa==2 || $empresa==3 || $empresa==6){
            $sql = "SELECT * FROM servicios WHERE familia=$familia /*and proveedor=$empresa*/ and servicios.status=1
            order by nombre ASC";
        }else if($empresa==4){
            $sql = "SELECT * FROM servicios_ahisa WHERE familia=$familia and status=1
            order by nombre ASC";
        }else if($empresa==5){
            $sql = "SELECT * FROM servicios_auven WHERE familia=$familia and status=1
            order by nombre ASC";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getOrdenes($params){
        $id_empresa=$params["id_empresa"];
        $id_cliente=$params["id_cliente"];
        if($id_empresa!="4" && $id_empresa!="5"){
            $table="servicios";
        }if($id_empresa=="4"){
            $table="servicios_ahisa";
        }if($id_empresa=="5"){
            $table="servicios_auven";
        }
        $columns = array( 
            0 => 'o.cotizacion_id',
            1 => 'alias',
            2 => 'o.fecha_creacion',
            //2 => 'date_format(o.fecha_creacion, "%d-%m-%Y") as fecha_creacion',
            3 => 'carpeta_prin',
            4 => 'o.file_repor',
            5 => 'carpeta',
            6 => 'cliente_id',
            //7 => "(select GROUP_CONCAT(s2.nombre SEPARATOR '<br>') as servicio from ".$table." as s2 where s2.id=chs.servicio_id and chs.id=orp.id_serv_repor) as servicio",
            //7 => "GROUP_CONCAT(s.nombre SEPARATOR '<br>') as servicio",
            /*7 => " GROUP_CONCAT(DISTINCT CONCAT_WS('<br>', 
                    IFNULL(s.nombre, ''),
                    IFNULL(s2.nombre, ''),
                    IFNULL(s3.nombre, '')
                ) SEPARATOR '<br>') as servicios",*/
            7 => 'concat(
                 IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5
                 order by orp.id asc),""),"<br>",
                 IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1 
                 join servicios_ahisa s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv=4
                 order by orp.id asc),""),"<br>",
                 IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios_auven s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv=5
                 order by orp.id asc),"")
             ) as servicios',
            /*7 => 'IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5),"") as servicio',
            8 => 'IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios_ahisa s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv=4),"") as servicio2',
            9 => 'IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios_auven s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv=5),"") as servicio3',
            */
            /*8 => "GROUP_CONCAT(DISTINCT CONCAT_WS('<br>', 
                    IFNULL(chs.servicio_id, ''),
                    IFNULL(chs2.servicio_id, ''),
                    IFNULL(chs3.servicio_id, '')
                ) SEPARATOR '<br>') as idservicios",*/
            8 => 'concat(
                 IFNULL((select GROUP_CONCAT(s.id SEPARATOR "<br>") from cotizaciones_has_servicios chs
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1 
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5
                 order by orp.id asc),""),"<br>",
                 IFNULL((select GROUP_CONCAT(s.id SEPARATOR "<br>") from cotizaciones_has_servicios chs
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1 
                 join servicios_ahisa s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv=4
                 order by orp.id asc),""),"<br>",
                 IFNULL((select GROUP_CONCAT(s.id SEPARATOR "<br>") from cotizaciones_has_servicios chs
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1 
                 join servicios_auven s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv=5
                 order by orp.id asc),"")
             ) as idservicios',

            /*10 => 'IFNULL((select GROUP_CONCAT(s.id SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5),"") as idservicio',
            11 => 'IFNULL((select GROUP_CONCAT(s.id SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios_ahisa s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv=4),"") as idservicio2',
            12 => 'IFNULL((select GROUP_CONCAT(s.id SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios_auven s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv=5),"") as idservicio3',
            */
            //13 => "GROUP_CONCAT(DISTINCT chs.servicio_id SEPARATOR '<br>') as servicio_idchs2",
            13 => "(select GROUP_CONCAT(chs.servicio_id SEPARATOR '<br>') as servicio_idchs2 from cotizaciones_has_servicios as chs where chs.cotizacion_id=c.id and chs.status=1 order by chs.id ASC) as servicio_idchs2",
            14 => "(select GROUP_CONCAT(chs.servicio_id SEPARATOR '<br>') as id_serv_carga from ordenes_reportes as orp 
                    join cotizaciones_has_servicios chs on chs.id=orp.id_serv_repor and chs.status=1
                    where orp.id_orden=o.id and orp.estatus=1 order by orp.id_serv_repor ASC) as id_serv_carga",
            15 => "GROUP_CONCAT(DISTINCT orp.id_serv_repor SEPARATOR '<br>') as id_serv_repor",
            //15 => "(select GROUP_CONCAT(orp.id SEPARATOR '<br>') as id_serv_repor from ordenes_reportes as orp where orp.id_orden=o.id and orp.estatus=1 and orp.id_serv_repor!=0 order by orp.id_serv_repor ASC) as id_serv_repor",
            16 => "GROUP_CONCAT(DISTINCT chs.id SEPARATOR '<br>') as servicio_idchs",
            //16 => "(select GROUP_CONCAT(chs.id SEPARATOR '<br>') as servicio_idchs from cotizaciones_has_servicios as chs where chs.cotizacion_id=c.id and chs.status=1 order by chs.id ASC) as servicio_idchs",
            //17 => "GROUP_CONCAT(s.id SEPARATOR '<br>') as id_servicio",
            18 => 'o.id',
            19 => 'chs.id_empresa_serv',
            /*20 => 'concat(
                 IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5),""),
                 IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios_ahisa s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv=4),""),
                 IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join servicios_auven s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.id_empresa_serv=5),"")
             ) as servicios',*/
        );

        $columns2 = array( 
            0 => 'o.cotizacion_id',
            1 => 'alias',
            2 => 'o.fecha_creacion',
            3 => 'carpeta_prin',
            4 => 'o.file_repor',
            5 => 'carpeta',
            6 => 'cliente_id',
            7 => 's.nombre',
            8 => 's2.nombre',
            9 => 's3.nombre'
            /*7 => 'IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5
                 order by chs.id asc),"")',
            8 => 'IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv=4
                 order by chs.id asc),"")',
            9 => 'IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=c.id and chs.status=1 and chs.id_empresa_serv=5
                 order by chs.id asc),"")',
            */
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select." ,date_format(o.fecha_creacion, '%d-%m-%Y') as fecha_creacion");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id');
        $this->db->join('ordenes_reportes orp', 'orp.id_orden=o.id and orp.estatus=1 and orp.id_serv_repor!=0');
        if($id_empresa=="4" || $id_empresa=="5"){
            $this->db->where('c.id_empresa',$id_empresa); 
        }else{
            $this->db->where('c.id_empresa IN (1, 2, 3, 6)');
        }
        //if($params["categoria"]=="0" && $params["servicio"]=="0"){
            //$this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1');
            $this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=c.id and chs.status=1');
            //$this->db->join($table. ' s','s.id=chs.servicio_id and chs.id=orp.id_serv_repor'); 

            $this->db->join('servicios s', 's.id = chs.servicio_id AND chs.id_empresa_serv NOT IN (4, 5)','left');
            $this->db->join('servicios_ahisa s2', 's2.id=chs.servicio_id and chs.id_empresa_serv=4','left');
            $this->db->join('servicios_auven s3', 's3.id=chs.servicio_id and chs.id_empresa_serv=5','left');

            ////$this->db->join('cotizaciones_has_servicios chs2', 'chs2.cotizacion_id = c.id AND chs2.status = 1 AND chs2.id_empresa_serv = 4','left');
            ////$this->db->join('cotizaciones_has_servicios chs3', 'chs3.cotizacion_id = c.id AND chs3.status = 1 AND chs3.id_empresa_serv = 5','left');

        /*}
        if($params["categoria"]!="0"){
            if($params["servicio"]=="0"){
                //$this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1');
                $this->db->join('cotizaciones_has_servicios chs','chs.id=orp.id_serv_repor and chs.status=1');
                $this->db->join($table. ' s','s.id=chs.servicio_id and familia='.$params["categoria"]); 
                //agregar where a serv/catego para condicionar listado
            }if($params["servicio"]!="0"){
                //$this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.servicio_id='.$params["servicio"].'');
                $this->db->join('cotizaciones_has_servicios chs','chs.id=orp.id_serv_repor and chs.status=1 and chs.servicio_id='.$params["servicio"].'');
                $this->db->join($table.'  s','s.id='.$params["servicio"].' and s.familia='.$params["categoria"]);
                $this->db->where('s.familia',$params["categoria"]);
            }
        }
        if($params["servicio"]!="0"){
            //$this->db->join('cotizaciones_has_servicios chs','chs.servicio_id='.$params["servicio"].'');
            //agregar where a serv/catego para condicionar listado 
        }*/

        //$this->db->where('c.id_empresa',$id_empresa);
        $this->db->where("c.cliente_id",$id_cliente); 
        $this->db->where("o.status",2); //terminada

        //$this->db->where("orp.file_repor!=''"); //con reporte
        $this->db->where("id_serv_repor > 0"); //con reporte y serv asignado
        $this->db->where("o.carpeta!=''");
        $this->db->where("(o.cargado=1 or o.carpeta_prin!='')");//con reporte
        
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            //$this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
            $this->db->where('year(o.fecha_creacion) >= '.$params["fechai"].' and year(o.fecha_creacion) <= '.$params["fechaf"].'');
        }else if($params["fechai"]!="" && $params["fechaf"]==""){
            //$this->db->where('o.fecha_creacion >= '.'"'.$params["fechai"].' 00:00:00"');
            $this->db->where('year(o.fecha_creacion) >= '.$params["fechai"].'');
        }
        $this->db->group_by("o.id");
        /*$this->db->order_by("s.id","desc");
        $this->db->order_by("s2.id","desc");
        $this->db->order_by("s3.id","desc");*/
        //$this->db->order_by("chs.servicio_id","asc");
        $this->db->order_by("orp.id","asc");
        //$this->db->order_by("o.cotizacion_id","desc");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }

    public function getTotalOrdenes($params){
        $id_empresa=$params["id_empresa"];
        $id_cliente=$params["id_cliente"];
        if($id_empresa!="4" && $id_empresa!="5"){
            $table="servicios";
        }if($id_empresa=="4"){
            $table="servicios_ahisa";
        }if($id_empresa=="5"){
            $table="servicios_auven";
        }
        $columns = array( 
            0 => 'o.cotizacion_id',
            1 => 'alias',
            2 => 'o.fecha_creacion',
            3 => 'carpeta_prin',
            4 => 'o.file_repor',
            5 => 'carpeta',
            6 => 'cliente_id',
            7 => 's.nombre',
            8 => 's2.nombre',
            9 => 's3.nombre'
        );
        $id_usuario=$this->session->userdata("id_usuario");
        $this->db->select("COUNT(1) as total");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id');
        $this->db->join('ordenes_reportes orp', 'orp.id_orden=o.id and orp.estatus=1 and orp.id_serv_repor!=0');
        if($id_empresa=="4" || $id_empresa=="5"){
            $this->db->where('c.id_empresa',$id_empresa); 
        }else{
            $this->db->where('c.id_empresa IN (1, 2, 3, 6)');
        }
        //if($params["categoria"]=="0" && $params["servicio"]=="0"){
            //$this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1');
            $this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=c.id and chs.status=1');
            //$this->db->join($table. ' s','s.id=chs.servicio_id and chs.id=orp.id_serv_repor'); 

            $this->db->join('servicios s', 's.id = chs.servicio_id AND chs.id_empresa_serv NOT IN (4, 5)','left');
            $this->db->join('servicios_ahisa s2', 's2.id=chs.servicio_id and chs.id_empresa_serv=4','left');
            $this->db->join('servicios_auven s3', 's3.id=chs.servicio_id and chs.id_empresa_serv=5','left');

            //$this->db->join('cotizaciones_has_servicios chs2', 'chs2.cotizacion_id = c.id AND chs2.status = 1 AND chs2.id_empresa_serv = 4','left');
            //$this->db->join('cotizaciones_has_servicios chs3', 'chs3.cotizacion_id = c.id AND chs3.status = 1 AND chs3.id_empresa_serv = 5','left');

        /*}
        if($params["categoria"]!="0"){
            if($params["servicio"]=="0"){
                //$this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1');
                $this->db->join('cotizaciones_has_servicios chs','chs.id=orp.id_serv_repor and chs.status=1');
                $this->db->join($table. ' s','s.id=chs.servicio_id and familia='.$params["categoria"]); 
                //agregar where a serv/catego para condicionar listado
            }if($params["servicio"]!="0"){
                //$this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.servicio_id='.$params["servicio"].'');
                $this->db->join('cotizaciones_has_servicios chs','chs.id=orp.id_serv_repor and chs.status=1 and chs.servicio_id='.$params["servicio"].'');
                $this->db->join($table.'  s','s.id='.$params["servicio"].' and s.familia='.$params["categoria"]);
                $this->db->where('s.familia',$params["categoria"]);
            }
        }
        if($params["servicio"]!="0"){
            //$this->db->join('cotizaciones_has_servicios chs','chs.servicio_id='.$params["servicio"].'');
            //agregar where a serv/catego para condicionar listado 
        }*/

        //$this->db->where('c.id_empresa',$id_empresa);
        $this->db->where("c.cliente_id",$id_cliente); 
        $this->db->where("o.status",2); //terminada

        //$this->db->where("orp.file_repor!=''"); //con reporte
        $this->db->where("id_serv_repor > 0"); //con reporte y serv asignado
        $this->db->where("o.carpeta!=''");
        $this->db->where("(o.cargado=1 or o.carpeta_prin!='')");//con reporte
        
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            //$this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
            $this->db->where('year(o.fecha_creacion) >= '.$params["fechai"].' and year(o.fecha_creacion) <= '.$params["fechaf"].'');
        }else if($params["fechai"]!="" && $params["fechaf"]==""){
            //$this->db->where('o.fecha_creacion >= '.'"'.$params["fechai"].' 00:00:00"');
            $this->db->where('year(o.fecha_creacion) >= '.$params["fechai"].'');
        }
        $this->db->group_by("o.id");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        } 

        $query=$this->db->get();
        $get=$query->result();
        $total=0;
        foreach($get as $k){
            $total++;
        }
        return $total;
    }

    public function get_reportes($id_ord,$id_repor){           
        $this->db->select("o.carpeta, o.carpeta_prin, or.id, or.file_repor");
        $this->db->from('ordenes o');
        $this->db->join('ordenes_reportes or', 'or.id_orden=o.id and or.estatus=1');
        $this->db->where("o.id",$id_ord);
        $this->db->where("or.id",$id_repor);
        $query=$this->db->get();
        return $query->row();
    }

    /*public function get_reportesDocs($id_ord){           
        $this->db->select("orp.*");
        $this->db->from('ordenes_reportes orp');
        $this->db->where("id_orden",$id_ord);
        $this->db->where("id_serv_repor > 0");
        $this->db->where("estatus",1);
        $this->db->order_by("id_serv_repor","ASC");
        $query=$this->db->get();
        return $query->result();
    }*/

    /*public function get_reportesDocs($id_ord){           
        $this->db->select("orp.*, chs.id_empresa_serv, IFNULL(s.nombre,'') as nombre, IFNULL(s2.nombre,'') as nombre2, IFNULL(s3.nombre,'') as nombre3, chs.id_empresa_serv, chs.servicio_id, chs.cotizacion_id");
        //$this->db->select("orp.*, chs.id_empresa_serv, GROUP_CONCAT(chs.id_empresa_serv SEPARATOR '<br>') as id_empresa_serv3,
            //GROUP_CONCAT(IFNULL(s.nombre, ''), IFNULL(s2.nombre, ''), IFNULL(s3.nombre, '') SEPARATOR '<br>') as nombre3, chs.id_empresa_serv");
        $this->db->from('ordenes_reportes orp');
        $this->db->join('ordenes o','o.id=orp.id_orden');
        $this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.id=orp.id_serv_repor');
        $this->db->join('servicios s', 's.id=chs.servicio_id and (chs.id_empresa_serv=1 or chs.id_empresa_serv=2 or chs.id_empresa_serv=3 or chs.id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa s2', 's2.id=chs.servicio_id and chs.id_empresa_serv=4','left');
        $this->db->join('servicios_auven s3', 's3.id=chs.servicio_id and chs.id_empresa_serv=5','left');
        $this->db->where("orp.id_orden",$id_ord);
        $this->db->where("orp.estatus",1);
        //$this->db->group_by("orp.id");
        $this->db->order_by("id_serv_repor","ASC");
        $query=$this->db->get();
        return $query->result();
    }
    */
    public function get_reportesDocs($id_ord,$id_serv_repor){           
        $this->db->select("orp.*, chs.id_empresa_serv, IFNULL(s.nombre,'') as nombre, IFNULL(s2.nombre,'') as nombre2, IFNULL(s3.nombre,'') as nombre3, chs.id_empresa_serv, chs.servicio_id, chs.cotizacion_id");
        $this->db->from('ordenes_reportes orp');
        $this->db->join('ordenes o','o.id=orp.id_orden');
        $this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.id=orp.id_serv_repor');
        $this->db->join('servicios s', 's.id=chs.servicio_id and (chs.id_empresa_serv=1 or chs.id_empresa_serv=2 or chs.id_empresa_serv=3 or chs.id_empresa_serv=6)','left');
        $this->db->join('servicios_ahisa s2', 's2.id=chs.servicio_id and chs.id_empresa_serv=4','left');
        $this->db->join('servicios_auven s3', 's3.id=chs.servicio_id and chs.id_empresa_serv=5','left');
        $this->db->where("orp.id_orden",$id_ord);
        //$this->db->where("orp.id",$id_serv_repor);
        $this->db->where("orp.estatus",1);
        //$this->db->group_by("orp.id");
        //$this->db->order_by("id_serv_repor","ASC");
        //$this->db->order_by("orp.id","asc");

        //$this->db->order_by("chs.id","asc");
        $this->db->order_by("chs.id_empresa_serv","asc");
        $this->db->order_by("orp.id","asc");
        //$this->db->order_by("chs.servicio_id","ASC");
        $query=$this->db->get();
        return $query->result();
    }

    /*public function get_reportesDocs($id_ord,$id_serv_repor){
        $this->db->select('orp.*, chs.id_empresa_serv,
                IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios s on s.id=chs.servicio_id
                 where chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.id=orp.id_serv_repor and chs.id_empresa_serv!=4 and chs.id_empresa_serv!=5
                 order by chs.servicio_id asc),"") as nombre,
                
                IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1 
                 join servicios_ahisa s on s.id=chs.servicio_id
                 where chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.id=orp.id_serv_repor and chs.id_empresa_serv=4
                 order by chs.servicio_id asc),"") as nombre2,
                
                IFNULL((select GROUP_CONCAT(s.nombre SEPARATOR "<br>") from cotizaciones_has_servicios chs 
                 join ordenes_reportes op on op.id_serv_repor=chs.id and op.estatus=1
                 join servicios_auven s on s.id=chs.servicio_id
                 where chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.id=orp.id_serv_repor and chs.id_empresa_serv=5
                 order by chs.servicio_id asc),"") as nombre3,
                chs.id_empresa_serv, chs.servicio_id, chs.cotizacion_id');
        $this->db->from('ordenes_reportes orp');
        $this->db->join('ordenes o','o.id=orp.id_orden');
        $this->db->join('cotizaciones_has_servicios chs','chs.cotizacion_id=o.cotizacion_id and chs.status=1 and chs.id=orp.id_serv_repor');
        
        $this->db->where("orp.id_orden",$id_ord);
        //$this->db->where("orp.id",$id_serv_repor);
        $this->db->where("orp.estatus",1);
        $this->db->order_by("chs.servicio_id","ASC");
        $query=$this->db->get();
        return $query->result();
    }*/

}
