<style type="text/css">
    .ocultar{ display:none; }
    .no_ocultar{ display:block; }
</style>
<div class="wrapper">
    <div class="main-container">
        <div class="mblank" style="margin-top: 40px;"></div>

        <div class="main-header">
            <h2 class="menu-link-main color_emp" href="javascript:void(0)">Bienvenido <b><?php echo $empresa; ?></b></h2>
            <!--<div class="header-menu">
                <a class="main-header-link is-active" href="javascript:void(0)">Listado</a>
            </div>-->
        </div>
        <!--<div class="">-->
        <div class="content-wrapper">
            <div class="">
                <div class="apps-card">
                    <div class="row col-md-12">
                        <input type="hidden" id="ide" value="<?php echo $id_empresa; ?>">
                        <div class="col-md-3 form-group">
                            <div class="position-relative has-icon-right">
                                <label class="content-section-title color_emp" for="fechai">Desde</label>
                                <!--<input id="fechai" type="date" class="form-control">-->
                                <select id="fechai" class="form-control form-control-sm"><?php echo $anios; ?></select>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <div class="position-relative has-icon-right">
                                <label class="content-section-title color_emp" for="fechaf">Hasta</label>
                                <!--<input id="fechaf" type="date" class="form-control">-->
                                <select id="fechaf" class="form-control form-control-sm"><?php echo $anios; ?></select>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <br>
                    </div>
                    <div class="col-md-12 table-responsive">
                        <table style="vertical-align:middle" class="table table-striped dataTables_wrapper no-footer color_emp" id="tabla" width="100%">
                            <thead style="text-align: center;">
                                <tr>
                                    <th width="8%">Cotización</th>
                                    <th width="17%">Cliente</th>
                                    <th width="9%">Fecha</th>
                                    <th width="49%">Servicio</th>
                                    <th width="17%">Resultados</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="overlay-app"></div>
</div>
