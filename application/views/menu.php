<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if($this->session->userdata('logeado') == true){ ?>
    <body class="<?php echo $body_emp; ?>">
        <div class="header">
            <!--<div class="menu-circle">
            </div>-->
            <div class=""><p></p>
            </div>
            <div class="content-wrapper-header">
                <div class="content-wrapper-context">
                    <img style="margin-left:0px; <?php echo $style; ?>" class="content-wrapper-img" src="<?php echo $logo; ?>" alt="">
                </div>
            </div>
            <!--<div class="header-menu ">
                <a style="color:#85929E" class="menu-link is-active" href="javascript:void(0)">Reportes</a>
            </div>-->
            <div class="search-bar">
                <!--<input type="text" placeholder="Search">-->
            </div>
            <div class="<?php /*echo $cla_hs;*/ ?> header-profile">
                <!--<a style="color: #fff" href="<?php echo base_url(); ?>Main/cerrar_sesion" class="menu-link is-active"><i class="ft-power mr-2"></i><span>Cerrar Sesión</span></a>-->
                <a href="<?php echo base_url(); ?>Main/cerrar_sesion" class="menu-link is-active "><i class="ft-power mr-2"></i><span style="<?php echo $colortxt; ?>">Cerrar Sesión</span></a>
            </div>
        </div>

<?php }else{
    redirect(base_url());
}
