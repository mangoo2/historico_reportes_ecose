        <footer class="footer footer-static footer-light">
            <p class="clearfix text-muted text-sm-center px-2" style="color: #000000 !important"><span>Copyright  &copy; <?php echo date("Y");?> <a href="https://www.mangoo.mx" target="_blank" class="text-bold-800 warning darken-2">MANGOO SOFTWARE</a>, All rights reserved. </span></p>
        </footer>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js"></script>
    </body> 
</html> 