<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - Historico Reportes</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link href="<?php echo base_url();?>app-assets/css/login.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>app-assets/js/login_efect.js"></script>
  </head>

  <body>
    <div class="background_auven"></div>
    <div class="login-container" id="loginContainer">
      <div class="login-header_auven"> 
        <img src="<?php echo base_url() ?>app-assets/img/logos/auven/logo_auvenbco2.png">
        <h2>Bienvenido a<span class="login-header_auven"> sistema de reportes Auven</span></h2>
        <!--<p>Inicio de sesión</p>-->
      </div>
      <div class="login-form">
        <input type="hidden" id="idemp" value="3">
        <input type="hidden" id="basuri" value="<?php echo base_url(); ?>">
        <form action="#" method="post">
          <div class="input-group_auven">
            <i class="fas fa-user"></i>
            <input type="text" name="usuario" id="usuario" placeholder="Usuario" required>
          </div>
          <div class="input-group_auven">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" id="password" placeholder="Password" required>
          </div>
          <div class="options">
            <label for="remember">
              <input type="checkbox" id="remember"> Remember me
            </label>
          </div>
          <button class="button_auven" type="button" id="login_btn">Iniciar sesión</button>
        </form>
        <br>
        <div class="alert bg-success" id="correct" style="display: none;">
          <strong>Correcto!</strong> Iniciando sesión . . .
        </div>
        <div class="alert bg-pink" id="error" style="display: none;">
          <strong>Error!</strong> Usuario y/o contraseña incorrectos 
        </div>
      </div>
    </div>
  </body>
</html>
