<!DOCTYPE html>
<html lang="en" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="Mangoo">
        <title>Sistema de Reportes</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>app-assets/img/iconA.png">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>app-assets/img/iconA.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
        <!--dataTables-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/jquery.dataTables.min.css">
        <!-- BEGIN APEX CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style_historico.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/select2/select2.min.css">

        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
    </head>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <style type="text/css">
        .dataTables_wrapper label { color:<?php echo $colortxt; ?> !important; }
        .dataTables_paginate a { color:<?php echo $colortxt; ?> !important; }
        table.dataTable tbody tr {
            background-color: transparent;
        }
        .dataTables_info{
            color: <?php echo $colortxt; ?> !important;
          }
          .paginate_button{
            color: <?php echo $colortxt; ?> !important;
          }
          .color_emp{
            color: <?php echo $colortxt; ?> !important
          }
        table{ font-size:12px; }
    </style>