<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Ordenes extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Ordenes_model');
        $this->id_empresa=$this->session->userdata("id_empresa");
        $this->logeado=$this->session->userdata('logeado');
        $this->id_cliente=$this->session->userdata("id_cliente");
        if($this->logeado) {

        }
        else{
            redirect(base_url(), 'refresh');
        }
    }

    public function inicio(){
        $data["empresa"]=$this->session->userdata("empresa");
        $data["id_empresa"]=$this->id_empresa;
        if($this->id_empresa==1){ //ecose
            $data["color"]="#13bf0d";
            //$data["logo"]=base_url()."app-assets/img/logos/ecose/logo2.png";
            $data["logo"]=base_url()."app-assets/img/logos/ecose/logo.png";
            $data["razon"] = "Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V.";
            $data["style"]="margin-top:-30px; max-width:110px";
            $data["body_emp"]="body_ecose";
            $datac["colortxt"]="#000000";
            //$datac["colortxt"]="#ffff";
            $datac["cla_hs"]="content-wrapper-headerecose";
            
            /*$data["color"]="#e86300";
            $data["logo"] = base_url()."app-assets/img/logos/ahisa/ahisa.png";
            $datac["cla_hs"]="content-wrapper-headerahisa";
            $data["style"]="margin-top:-30px; max-width:180px;";*/

            /*$data["color"]="#0a3971";
            $data["logo"]=base_url()."app-assets/img/logos/auven/logo.png";
            $data["style"]="margin-top:-30px; max-width:110px;";
            $datac["cla_hs"]="content-wrapper-headerauven";
            $datac["colortxt"]="#000000";
            $data["colortxt"]="#000000";*/
        }
        else if($this->id_empresa=="4") {
            $data["color"]="#e86300";
            $data["logo"] = base_url()."app-assets/img/logos/ahisa/ahisa.png";
            $data["razon"] = "AHISA Laboratorio de Pruebas S. de R.L. de C.V.";  
            $data["style"]="margin-top:-30px; max-width:180px";
            //$data["body_emp"]="body_ahisa";
            $data["body_emp"]="body_ecose";
            $datac["colortxt"]="#000000";
            //$datac["colortxt"]="#ffff";
            $datac["cla_hs"]="content-wrapper-headerahisa";
        }
        else if($this->id_empresa=="5") {
            $data["color"]="#0a3971";
            $data["logo"] = base_url()."app-assets/img/logos/auven/logo.png";
            $data["razon"] = "AUVEN S. de R.L. de C.V.";
            $data["style"]="margin-top:-30px; max-width:110px;";
            //$data["body_emp"]="body_auven";
            $data["body_emp"]="body_ecose";
            $datac["colortxt"]="#000000";
            //$datac["colortxt"]="#ffff";
            $datac["cla_hs"]="content-wrapper-headerauven";
        }
        $data["anios"]=""; $ai=date("Y", strtotime("- 10 years")); $af=date("Y");
        for($i=$ai; $i<=$af; $i++){
            $sel="";
            if($i==date("Y")){
                $sel="selected";
            }
            $data["anios"].="<option ".$sel." value='".$i."'>".$i."</option>";
        }
        $this->load->view('header',$datac);
        $this->load->view('menu',$data);
        $this->load->view('inicio',$data);
        $this->load->view('footer');
        $this->load->view('historico_ordenes');
    }
	
    public function getData_ordenes() { //tabla de ordenes de trabajo
        $params=$this->input->post();
        $params["id_empresa"]=$this->id_empresa;
        $params["id_cliente"]=$this->id_cliente;
        $ordenes = $this->Ordenes_model->getOrdenes($params);
        $totalRecords=$this->Ordenes_model->getTotalOrdenes($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $ordenes->result(),
            "query"           =>$this->db->last_query()   
            );
        echo json_encode($json_data);
    }

    function searchFamilias(){
        $pro = $this->input->get('search');
        $familias=$this->Ordenes_model->searchFamilias($this->id_empresa);
        echo json_encode($familias);
    }

    function searchServicios(){
        $pro = $this->input->get('search');
        $fam = $this->input->get('fam');
        $servicios=$this->Ordenes_model->searchServicios($fam,$this->id_empresa);
        echo json_encode($servicios);
    }

    function getReporte(){
        $id_ord = $this->input->post('id_ord');
        $id_repor = $this->input->post('id_repor');
        $get=$this->Ordenes_model->get_reportes($id_ord,$id_repor);
        echo json_encode(array("carpeta_prin"=>$get->carpeta_prin,"carpeta"=>$get->carpeta,"file_repor"=>$get->file_repor));
    }

    function getReportesDocs(){
        $id = $this->input->post('id_ord');
        $id_serv_repor = $this->input->post('id_serv_repor');
        $result=$this->Ordenes_model->get_reportesDocs($id,$id_serv_repor);
        echo json_encode(array("datos"=>$result));   
    }
    
}