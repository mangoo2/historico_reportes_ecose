<?php

class Ordenes_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo) {
        $sql = "SELECT * FROM $catalogo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getOrden($id){
        //$this->db->set("lc_time_names = 'es_ES'");
        //$this->db->select("SET o.fecha_creacion = 'es_ES'");
        $this->db->select("o.*, MONTHNAME(o.fecha_creacion) as mes, MONTH(o.fecha_creacion) as mesn, YEAR(o.fecha_creacion) as anio, DAY(o.fecha_creacion) as dia,
           ");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('empleados e', 'e.id = c.vendedor_id');
        $this->db->where("o.id",$id);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function getCliente($id){
        $this->db->select("*");
        $this->db->from('clientes c');
        $this->db->join('personas_contacto p', 'c.id = p.cliente_id');
        $this->db->where("c.id",$id);
        $this->db->where("c.estatus",1);
        $query=$this->db->get();
        return $query->row();
    }
	
	public function updateOrden($id,$data){
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('ordenes');
    }
    
       
    public function getOrdenes($params,$sta){
        $id_usuario=$this->session->userdata("id_usuario");
        $columns = array( 
            0 => 'o.id',
            1 => 'o.cotizacion_id',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            5 => 'empleados.usuario',
            6 => 'o.status',
            7 => 'o.id'
        );
        $columns2 = array( 
            0 => 'o.id',
            1 => 'o.cotizacion_id',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'cl.alias',
            5 => 'empleados.nombre',
            6 => 'o.status',
            7 => 'o.id'
        );
        
        if(!$this->session->userdata("administrador")){
            $columns[2]='CONCAT(s.nombre,"<br>",s.descripcion) as nombre';
            //$where = "and id_usuario = $id_usuario"; 
        }/*else
            $where="";*/
        
        /*$select="o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status,";
        $select.=", (SELECT usuario FROM empleados where id=c.vendedor_id) as vendedor, (SELECT nombre FROM empleados where id=c.vendedor_id) as nomVendedor,";*/
        $select = "o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status, empleados.usuario as nomVendedor, o.cotizacion_id as nombre, ";
        
        if(!$this->session->userdata("administrador") && $this->session->userdata("perfil")!='3'){
            $select.="CONCAT(s.nombre,'<br>',s.descripcion) as nombre,cs.id as servicio_id,fecha_inicio,fecha_fin,hora_inicio,hora_fin,cs.comentarios, ";
        }
        else{
            $select.="o.cotizacion_id as nombre, ";
            $select.="";
        }
             
        $this->db->select($select);
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados', 'empleados.id = c.vendedor_id','left');


        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y tecnico
            $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
            $this->db->join('servicios s', 's.id = cs.servicio_id');
            
            $this->db->where("s.proveedor",$this->session->userdata("empresa"));
            $this->db->where('cs.status!=', 0);
            $this->db->where('s.status!=', 0);
        }

        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){  //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){  //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
        }
        if($sta!=0){
            $this->db->where("o.status",$sta);
        }else{
            $this->db->where("o.status!=","0");
        }
        //$this->db->where('cl.status!=',0);
        $this->db->where('c.status!=',0);
        
        //$this->db->order_by('o.id', 'DESC');
        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }

        //$this->db->group_by('o.id');
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }

    public function getTotalOrdenes(){
        $id_usuario=$this->session->userdata("id_usuario");
        $this->db->select("count(1) as total");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        
        if(!$this->session->userdata("administrador")){
            
            $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
            $this->db->join('servicios s', 's.id = cs.servicio_id');
            $this->db->where("s.proveedor",$this->session->userdata("empresa"));
            //$this->db->where("o.id_usuario",$id_usuario);
        }
        $this->db->where("o.status!=",0);
        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){ //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
        }
        //$this->db->group_by('o.id');
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getTotalOrdenesEstatus($status){
        $id_usuario=$this->session->userdata("id_usuario");
        $this->db->select("count(1) as total");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        
        if(!$this->session->userdata("administrador")){
            
            $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
            $this->db->join('servicios s', 's.id = cs.servicio_id');
            $this->db->where("s.proveedor",$this->session->userdata("empresa"));
            //$this->db->where("o.status",$sta);
            //$this->db->where("o.status!=",0);
            //$this->db->where("o.id_usuario",$id_usuario);
        }
        /*if($id_usuario>1 && $this->session->userdata("departamento")!="DEPARTAMENTO TECNICO"){ //descomentar despues de que asignen a c/usu
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario"));
        }*/
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){
            $this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
        }
        $this->db->where("o.status",$status);
        //$this->db->where("o.status!=",0);
        $query=$this->db->get();
        return $query->row()->total;
    }
    
    public function getServiciosCotizacion($id){
        $this->db->select("cotizaciones_has_servicios.*,servicios.clave,servicios.nombre,servicios.descripcion,prov.nombre as empresa");
        $this->db->join('servicios', 'servicios.id = servicio_id');
        $this->db->join('proveedores as prov', 'proveedor = prov.id');
        $this->db->where("cotizacion_id",$id);
        if(!$this->session->userdata("administrador")){
            $this->db->where("servicios.proveedor",$this->session->userdata("empresa"));
        }
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }
    
    public function getServicio($id){
        $this->db->select("servicios.nombre as nombre, empleados.correo");
        $this->db->join('servicios', 'servicios.id = servicio_id');
        $this->db->join('empleados', 'empleados.id = servicios.proveedor');
        $this->db->where("cotizaciones_has_servicios.id",$id);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->row()->nombre;
    }
    public function getDocsFam($id){
        $this->db->select("ordenes.familia_id, familias_has_documentos.documento");
        //$this->db->from("ordenes");
        $this->db->join('familias_has_documentos', 'familias_has_documentos.familia_id = ordenes.id');
        $this->db->where("ordenes.id",$id);
        $query=$this->db->get("ordenes");
        return $query->row()->documento;
    }
    
    public function getMail($id){
        $this->db->select("email");
        $this->db->join('cotizaciones', 'cotizaciones.id = cotizacion_id');
        $this->db->join('personas_contacto', 'cotizaciones.cliente_id = personas_contacto.cliente_id');
        $this->db->where("cotizaciones_has_servicios.id",$id);
        $this->db->where("orden",1);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->row()->email;
    }
    
}